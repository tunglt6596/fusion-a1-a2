-- localedgepreserve_GN_localedgepreserve_Fusion_Cal_Image_Out_1_MeanA_multiplier_accumulator.vhd

-- Generated using ACDS version 17.1 590

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity localedgepreserve_GN_localedgepreserve_Fusion_Cal_Image_Out_1_MeanA_multiplier_accumulator is
	port (
		data_12 : in  std_logic_vector(15 downto 0) := (others => '0'); -- data_12.wire
		Clock   : in  std_logic                     := '0';             --   Clock.clk
		reset   : in  std_logic                     := '0';             --        .reset
		data_21 : in  std_logic_vector(15 downto 0) := (others => '0'); -- data_21.wire
		data_01 : in  std_logic_vector(15 downto 0) := (others => '0'); -- data_01.wire
		data_20 : in  std_logic_vector(15 downto 0) := (others => '0'); -- data_20.wire
		data_22 : in  std_logic_vector(15 downto 0) := (others => '0'); -- data_22.wire
		clken   : in  std_logic                     := '0';             --   clken.wire
		data_02 : in  std_logic_vector(15 downto 0) := (others => '0'); -- data_02.wire
		a_out   : out std_logic_vector(7 downto 0);                     --   a_out.wire
		data_10 : in  std_logic_vector(15 downto 0) := (others => '0'); -- data_10.wire
		data_11 : in  std_logic_vector(15 downto 0) := (others => '0'); -- data_11.wire
		data_00 : in  std_logic_vector(15 downto 0) := (others => '0')  -- data_00.wire
	);
end entity localedgepreserve_GN_localedgepreserve_Fusion_Cal_Image_Out_1_MeanA_multiplier_accumulator;

architecture rtl of localedgepreserve_GN_localedgepreserve_Fusion_Cal_Image_Out_1_MeanA_multiplier_accumulator is
	component alt_dspbuilder_clock_GNQFU4PUDH is
		port (
			aclr      : in  std_logic := 'X'; -- reset
			aclr_n    : in  std_logic := 'X'; -- reset_n
			aclr_out  : out std_logic;        -- reset
			clock     : in  std_logic := 'X'; -- clk
			clock_out : out std_logic         -- clk
		);
	end component alt_dspbuilder_clock_GNQFU4PUDH;

	component alt_dspbuilder_multiplier_GN4OQUTWTO is
		generic (
			aWidth                         : natural := 8;
			Signed                         : natural := 0;
			bWidth                         : natural := 8;
			DEDICATED_MULTIPLIER_CIRCUITRY : string  := "AUTO";
			pipeline                       : integer := 0;
			OutputLsb                      : integer := 0;
			OutputMsb                      : integer := 8
		);
		port (
			aclr      : in  std_logic                                          := 'X';             -- clk
			clock     : in  std_logic                                          := 'X';             -- clk
			dataa     : in  std_logic_vector(aWidth-1 downto 0)                := (others => 'X'); -- wire
			datab     : in  std_logic_vector(bWidth-1 downto 0)                := (others => 'X'); -- wire
			ena       : in  std_logic                                          := 'X';             -- wire
			result    : out std_logic_vector(OutputMsb-OutputLsb+1-1 downto 0);                    -- wire
			user_aclr : in  std_logic                                          := 'X'              -- wire
		);
	end component alt_dspbuilder_multiplier_GN4OQUTWTO;

	component alt_dspbuilder_gnd_GN is
		port (
			output : out std_logic   -- wire
		);
	end component alt_dspbuilder_gnd_GN;

	component alt_dspbuilder_parallel_adder_GNRITCUGPT is
		generic (
			dataWidth     : positive := 8;
			direction     : string   := "+";
			MaskValue     : string   := "1";
			pipeline      : natural  := 0;
			number_inputs : positive := 2
		);
		port (
			clock     : in  std_logic                     := 'X';             -- clk
			aclr      : in  std_logic                     := 'X';             -- reset
			result    : out std_logic_vector(20 downto 0);                    -- wire
			user_aclr : in  std_logic                     := 'X';             -- wire
			ena       : in  std_logic                     := 'X';             -- wire
			data0     : in  std_logic_vector(16 downto 0) := (others => 'X'); -- wire
			data1     : in  std_logic_vector(16 downto 0) := (others => 'X'); -- wire
			data2     : in  std_logic_vector(16 downto 0) := (others => 'X'); -- wire
			data3     : in  std_logic_vector(16 downto 0) := (others => 'X'); -- wire
			data4     : in  std_logic_vector(16 downto 0) := (others => 'X'); -- wire
			data5     : in  std_logic_vector(16 downto 0) := (others => 'X'); -- wire
			data6     : in  std_logic_vector(16 downto 0) := (others => 'X'); -- wire
			data7     : in  std_logic_vector(16 downto 0) := (others => 'X'); -- wire
			data8     : in  std_logic_vector(16 downto 0) := (others => 'X')  -- wire
		);
	end component alt_dspbuilder_parallel_adder_GNRITCUGPT;

	component alt_dspbuilder_constant_GNUTAAMD7E is
		generic (
			BitPattern : string  := "0000";
			HDLTYPE    : string  := "STD_LOGIC_VECTOR";
			width      : natural := 4
		);
		port (
			output : out std_logic_vector(23 downto 0)   -- wire
		);
	end component alt_dspbuilder_constant_GNUTAAMD7E;

	component alt_dspbuilder_port_GN37ALZBS4 is
		port (
			input  : in  std_logic := 'X'; -- wire
			output : out std_logic         -- wire
		);
	end component alt_dspbuilder_port_GN37ALZBS4;

	component alt_dspbuilder_port_GNBO6OMO5Y is
		port (
			input  : in  std_logic_vector(15 downto 0) := (others => 'X'); -- wire
			output : out std_logic_vector(15 downto 0)                     -- wire
		);
	end component alt_dspbuilder_port_GNBO6OMO5Y;

	component alt_dspbuilder_port_GNA5S4SQDN is
		port (
			input  : in  std_logic_vector(7 downto 0) := (others => 'X'); -- wire
			output : out std_logic_vector(7 downto 0)                     -- wire
		);
	end component alt_dspbuilder_port_GNA5S4SQDN;

	component alt_dspbuilder_cast_GN6RUFTBHU is
		generic (
			round    : natural := 0;
			saturate : natural := 0
		);
		port (
			input  : in  std_logic_vector(20 downto 0) := (others => 'X'); -- wire
			output : out std_logic_vector(11 downto 0)                     -- wire
		);
	end component alt_dspbuilder_cast_GN6RUFTBHU;

	component alt_dspbuilder_cast_GNRISZPI4K is
		generic (
			round    : natural := 0;
			saturate : natural := 0
		);
		port (
			input  : in  std_logic_vector(15 downto 0) := (others => 'X'); -- wire
			output : out std_logic_vector(16 downto 0)                     -- wire
		);
	end component alt_dspbuilder_cast_GNRISZPI4K;

	signal multiplier2user_aclrgnd_output_wire               : std_logic;                     -- Multiplier2user_aclrGND:output -> Multiplier2:user_aclr
	signal parallel_adder_subtractoruser_aclrgnd_output_wire : std_logic;                     -- Parallel_Adder_Subtractoruser_aclrGND:output -> Parallel_Adder_Subtractor:user_aclr
	signal constant2_output_wire                             : std_logic_vector(23 downto 0); -- Constant2:output -> Multiplier2:datab
	signal clken_0_output_wire                               : std_logic;                     -- clken_0:output -> [Multiplier2:ena, Parallel_Adder_Subtractor:ena]
	signal multiplier2_result_wire                           : std_logic_vector(7 downto 0);  -- Multiplier2:result -> a_out_0:input
	signal parallel_adder_subtractor_result_wire             : std_logic_vector(20 downto 0); -- Parallel_Adder_Subtractor:result -> cast31:input
	signal cast31_output_wire                                : std_logic_vector(11 downto 0); -- cast31:output -> Multiplier2:dataa
	signal data_00_0_output_wire                             : std_logic_vector(15 downto 0); -- data_00_0:output -> cast32:input
	signal cast32_output_wire                                : std_logic_vector(16 downto 0); -- cast32:output -> Parallel_Adder_Subtractor:data0
	signal data_01_0_output_wire                             : std_logic_vector(15 downto 0); -- data_01_0:output -> cast33:input
	signal cast33_output_wire                                : std_logic_vector(16 downto 0); -- cast33:output -> Parallel_Adder_Subtractor:data1
	signal data_02_0_output_wire                             : std_logic_vector(15 downto 0); -- data_02_0:output -> cast34:input
	signal cast34_output_wire                                : std_logic_vector(16 downto 0); -- cast34:output -> Parallel_Adder_Subtractor:data2
	signal data_10_0_output_wire                             : std_logic_vector(15 downto 0); -- data_10_0:output -> cast35:input
	signal cast35_output_wire                                : std_logic_vector(16 downto 0); -- cast35:output -> Parallel_Adder_Subtractor:data3
	signal data_11_0_output_wire                             : std_logic_vector(15 downto 0); -- data_11_0:output -> cast36:input
	signal cast36_output_wire                                : std_logic_vector(16 downto 0); -- cast36:output -> Parallel_Adder_Subtractor:data4
	signal data_12_0_output_wire                             : std_logic_vector(15 downto 0); -- data_12_0:output -> cast37:input
	signal cast37_output_wire                                : std_logic_vector(16 downto 0); -- cast37:output -> Parallel_Adder_Subtractor:data5
	signal data_20_0_output_wire                             : std_logic_vector(15 downto 0); -- data_20_0:output -> cast38:input
	signal cast38_output_wire                                : std_logic_vector(16 downto 0); -- cast38:output -> Parallel_Adder_Subtractor:data6
	signal data_21_0_output_wire                             : std_logic_vector(15 downto 0); -- data_21_0:output -> cast39:input
	signal cast39_output_wire                                : std_logic_vector(16 downto 0); -- cast39:output -> Parallel_Adder_Subtractor:data7
	signal data_22_0_output_wire                             : std_logic_vector(15 downto 0); -- data_22_0:output -> cast40:input
	signal cast40_output_wire                                : std_logic_vector(16 downto 0); -- cast40:output -> Parallel_Adder_Subtractor:data8
	signal clock_0_clock_output_clk                          : std_logic;                     -- Clock_0:clock_out -> [Multiplier2:clock, Parallel_Adder_Subtractor:clock]
	signal clock_0_clock_output_reset                        : std_logic;                     -- Clock_0:aclr_out -> [Multiplier2:aclr, Parallel_Adder_Subtractor:aclr]

begin

	clock_0 : component alt_dspbuilder_clock_GNQFU4PUDH
		port map (
			clock_out => clock_0_clock_output_clk,   -- clock_output.clk
			aclr_out  => clock_0_clock_output_reset, --             .reset
			clock     => Clock,                      --        clock.clk
			aclr      => reset                       --             .reset
		);

	multiplier2 : component alt_dspbuilder_multiplier_GN4OQUTWTO
		generic map (
			aWidth                         => 12,
			Signed                         => 0,
			bWidth                         => 24,
			DEDICATED_MULTIPLIER_CIRCUITRY => "YES",
			pipeline                       => 1,
			OutputLsb                      => 24,
			OutputMsb                      => 31
		)
		port map (
			clock     => clock_0_clock_output_clk,            -- clock_aclr.clk
			aclr      => clock_0_clock_output_reset,          --           .reset
			dataa     => cast31_output_wire,                  --      dataa.wire
			datab     => constant2_output_wire,               --      datab.wire
			result    => multiplier2_result_wire,             --     result.wire
			user_aclr => multiplier2user_aclrgnd_output_wire, --  user_aclr.wire
			ena       => clken_0_output_wire                  --        ena.wire
		);

	multiplier2user_aclrgnd : component alt_dspbuilder_gnd_GN
		port map (
			output => multiplier2user_aclrgnd_output_wire  -- output.wire
		);

	parallel_adder_subtractor : component alt_dspbuilder_parallel_adder_GNRITCUGPT
		generic map (
			dataWidth     => 17,
			direction     => "+",
			MaskValue     => "1",
			pipeline      => 1,
			number_inputs => 9
		)
		port map (
			clock     => clock_0_clock_output_clk,                          -- clock_aclr.clk
			aclr      => clock_0_clock_output_reset,                        --           .reset
			result    => parallel_adder_subtractor_result_wire,             --     result.wire
			user_aclr => parallel_adder_subtractoruser_aclrgnd_output_wire, --  user_aclr.wire
			ena       => clken_0_output_wire,                               --        ena.wire
			data0     => cast32_output_wire,                                --      data0.wire
			data1     => cast33_output_wire,                                --      data1.wire
			data2     => cast34_output_wire,                                --      data2.wire
			data3     => cast35_output_wire,                                --      data3.wire
			data4     => cast36_output_wire,                                --      data4.wire
			data5     => cast37_output_wire,                                --      data5.wire
			data6     => cast38_output_wire,                                --      data6.wire
			data7     => cast39_output_wire,                                --      data7.wire
			data8     => cast40_output_wire                                 --      data8.wire
		);

	parallel_adder_subtractoruser_aclrgnd : component alt_dspbuilder_gnd_GN
		port map (
			output => parallel_adder_subtractoruser_aclrgnd_output_wire  -- output.wire
		);

	constant2 : component alt_dspbuilder_constant_GNUTAAMD7E
		generic map (
			BitPattern => "000111000111000111000111",
			HDLTYPE    => "STD_LOGIC_VECTOR",
			width      => 24
		)
		port map (
			output => constant2_output_wire  -- output.wire
		);

	clken_0 : component alt_dspbuilder_port_GN37ALZBS4
		port map (
			input  => clken,               --  input.wire
			output => clken_0_output_wire  -- output.wire
		);

	data_00_0 : component alt_dspbuilder_port_GNBO6OMO5Y
		port map (
			input  => data_00,               --  input.wire
			output => data_00_0_output_wire  -- output.wire
		);

	data_11_0 : component alt_dspbuilder_port_GNBO6OMO5Y
		port map (
			input  => data_11,               --  input.wire
			output => data_11_0_output_wire  -- output.wire
		);

	data_22_0 : component alt_dspbuilder_port_GNBO6OMO5Y
		port map (
			input  => data_22,               --  input.wire
			output => data_22_0_output_wire  -- output.wire
		);

	data_10_0 : component alt_dspbuilder_port_GNBO6OMO5Y
		port map (
			input  => data_10,               --  input.wire
			output => data_10_0_output_wire  -- output.wire
		);

	data_21_0 : component alt_dspbuilder_port_GNBO6OMO5Y
		port map (
			input  => data_21,               --  input.wire
			output => data_21_0_output_wire  -- output.wire
		);

	data_02_0 : component alt_dspbuilder_port_GNBO6OMO5Y
		port map (
			input  => data_02,               --  input.wire
			output => data_02_0_output_wire  -- output.wire
		);

	data_01_0 : component alt_dspbuilder_port_GNBO6OMO5Y
		port map (
			input  => data_01,               --  input.wire
			output => data_01_0_output_wire  -- output.wire
		);

	data_12_0 : component alt_dspbuilder_port_GNBO6OMO5Y
		port map (
			input  => data_12,               --  input.wire
			output => data_12_0_output_wire  -- output.wire
		);

	a_out_0 : component alt_dspbuilder_port_GNA5S4SQDN
		port map (
			input  => multiplier2_result_wire, --  input.wire
			output => a_out                    -- output.wire
		);

	data_20_0 : component alt_dspbuilder_port_GNBO6OMO5Y
		port map (
			input  => data_20,               --  input.wire
			output => data_20_0_output_wire  -- output.wire
		);

	cast31 : component alt_dspbuilder_cast_GN6RUFTBHU
		generic map (
			round    => 0,
			saturate => 0
		)
		port map (
			input  => parallel_adder_subtractor_result_wire, --  input.wire
			output => cast31_output_wire                     -- output.wire
		);

	cast32 : component alt_dspbuilder_cast_GNRISZPI4K
		generic map (
			round    => 0,
			saturate => 0
		)
		port map (
			input  => data_00_0_output_wire, --  input.wire
			output => cast32_output_wire     -- output.wire
		);

	cast33 : component alt_dspbuilder_cast_GNRISZPI4K
		generic map (
			round    => 0,
			saturate => 0
		)
		port map (
			input  => data_01_0_output_wire, --  input.wire
			output => cast33_output_wire     -- output.wire
		);

	cast34 : component alt_dspbuilder_cast_GNRISZPI4K
		generic map (
			round    => 0,
			saturate => 0
		)
		port map (
			input  => data_02_0_output_wire, --  input.wire
			output => cast34_output_wire     -- output.wire
		);

	cast35 : component alt_dspbuilder_cast_GNRISZPI4K
		generic map (
			round    => 0,
			saturate => 0
		)
		port map (
			input  => data_10_0_output_wire, --  input.wire
			output => cast35_output_wire     -- output.wire
		);

	cast36 : component alt_dspbuilder_cast_GNRISZPI4K
		generic map (
			round    => 0,
			saturate => 0
		)
		port map (
			input  => data_11_0_output_wire, --  input.wire
			output => cast36_output_wire     -- output.wire
		);

	cast37 : component alt_dspbuilder_cast_GNRISZPI4K
		generic map (
			round    => 0,
			saturate => 0
		)
		port map (
			input  => data_12_0_output_wire, --  input.wire
			output => cast37_output_wire     -- output.wire
		);

	cast38 : component alt_dspbuilder_cast_GNRISZPI4K
		generic map (
			round    => 0,
			saturate => 0
		)
		port map (
			input  => data_20_0_output_wire, --  input.wire
			output => cast38_output_wire     -- output.wire
		);

	cast39 : component alt_dspbuilder_cast_GNRISZPI4K
		generic map (
			round    => 0,
			saturate => 0
		)
		port map (
			input  => data_21_0_output_wire, --  input.wire
			output => cast39_output_wire     -- output.wire
		);

	cast40 : component alt_dspbuilder_cast_GNRISZPI4K
		generic map (
			round    => 0,
			saturate => 0
		)
		port map (
			input  => data_22_0_output_wire, --  input.wire
			output => cast40_output_wire     -- output.wire
		);

end architecture rtl; -- of localedgepreserve_GN_localedgepreserve_Fusion_Cal_Image_Out_1_MeanA_multiplier_accumulator
