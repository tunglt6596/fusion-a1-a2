# Graduation Project - IT5220 - SoICT - HUST

* Study to use PCIe IP Core, SG-DMA IP Core,... with Intel FPGAs.
* Write IP Cores to implement fusion algorithms with input images 640x480, fixed-point numbers, compatible with Avalon video streaming interface.  
* Display the output video via VGA, run experiments with 3 different datasets.

## Getting Started

* A1, A2: Quartus project, I've used 17.1 version to build it, you can use both Lite/Standard Edition versions without any problem. Note: A1 is algorithm 1 called Gaussian pyramid decomposition, A2 is algorithm 2 called LEP filter & Saliency extraction. 
* PCIE_DRIVER: PCIe device driver to control SG-DMA, Frame Reader... and the application to run experiments.

### Prerequisites

* I use Ubuntu 14.04 OS for my DE2i-150 board. The kernel version is 4.4.0. You can check your kernel version with this command:

```
$ uname -r
``` 

* Please add this two options into grub booting commands:

```
/etc/grub.conf: vmalloc=512MB intel_iommu=off
```

* With Ubuntu 14.04, you change this line in /etc/default/grub:

```
GRUB_CMDLINE_LINUX="vmalloc=512MB intel_iommu=off"
``` 

* Then update grub:

```
sudo update-grub
```

* If you're using another kernel version, maybe you need to modify device driver source. To build the device devicer:

```
$ cd driver
$ make
```

* To build application:

```
$ cd app
$ make
```

### Installing

* Run quick_start.sh with sudo permission to install the device driver and create a device file automatically.
```
$ sudo ./quick_start.sh
```

* Or you can install the device driver and create a device file manually.
```
$ cd driver
$ sudo insmod altera_driver.ko
$ sudo mknod /dev/de2i150_altera c 91 1
$ sudo chmod 777 /dev/de2i150_altera
```

* Go to PCIE_Driver/app, run the app.
```
$ cd app
$ sudo ./app
```

* Connect screen to FPGA's VGA port, you'll see the results.

## Deployment

* Some experimental videos:

```
https://www.youtube.com/watch?v=tnaQxFX1HJU
```

```
https://www.youtube.com/watch?v=0jAWMN0wZTA
```

```
https://www.youtube.com/watch?v=iMEFsiSaAMQ
```

## Versioning

* 1.0

## Authors

* Student Tung Thanh Le and lecturer Tien Duc Nguyen. 

## License

* This project is not licenced.

## Acknowledgments

* MsC Duc Tien Nguyen, who supervised and supported me in this project.

