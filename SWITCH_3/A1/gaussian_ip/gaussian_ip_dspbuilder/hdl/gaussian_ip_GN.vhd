-- gaussian_ip_GN.vhd

-- Generated using ACDS version 17.1 590

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity gaussian_ip_GN is
	port (
		source_eop   : out std_logic;                                        --   source_eop.wire
		sink_sop2    : in  std_logic                     := '0';             --    sink_sop2.wire
		Clock        : in  std_logic                     := '0';             --        Clock.clk
		reset        : in  std_logic                     := '0';             --             .reset
		source_sop   : out std_logic;                                        --   source_sop.wire
		source_valid : out std_logic;                                        -- source_valid.wire
		sink_valid1  : in  std_logic                     := '0';             --  sink_valid1.wire
		sink_eop2    : in  std_logic                     := '0';             --    sink_eop2.wire
		sink_ready2  : out std_logic;                                        --  sink_ready2.wire
		source_data  : out std_logic_vector(23 downto 0);                    --  source_data.wire
		sink_valid2  : in  std_logic                     := '0';             --  sink_valid2.wire
		sink_data1   : in  std_logic_vector(7 downto 0)  := (others => '0'); --   sink_data1.wire
		sink_ready1  : out std_logic;                                        --  sink_ready1.wire
		switch       : in  std_logic_vector(1 downto 0)  := (others => '0'); --       switch.wire
		source_ready : in  std_logic                     := '0';             -- source_ready.wire
		sink_data2   : in  std_logic_vector(7 downto 0)  := (others => '0'); --   sink_data2.wire
		sink_eop1    : in  std_logic                     := '0';             --    sink_eop1.wire
		sink_sop1    : in  std_logic                     := '0'              --    sink_sop1.wire
	);
end entity gaussian_ip_GN;

architecture rtl of gaussian_ip_GN is
	component alt_dspbuilder_clock_GNN7TLRCSZ is
		port (
			aclr      : in  std_logic := 'X'; -- reset
			aclr_n    : in  std_logic := 'X'; -- reset_n
			aclr_out  : out std_logic;        -- reset
			clock     : in  std_logic := 'X'; -- clk
			clock_out : out std_logic         -- clk
		);
	end component alt_dspbuilder_clock_GNN7TLRCSZ;

	component alt_dspbuilder_port_GN37ALZBS4 is
		port (
			input  : in  std_logic := 'X'; -- wire
			output : out std_logic         -- wire
		);
	end component alt_dspbuilder_port_GN37ALZBS4;

	component alt_dspbuilder_bus_concat_GNIIOZRPJD is
		generic (
			widthA : natural := 8;
			widthB : natural := 8
		);
		port (
			a      : in  std_logic_vector(widthA-1 downto 0)        := (others => 'X'); -- wire
			aclr   : in  std_logic                                  := 'X';             -- clk
			b      : in  std_logic_vector(widthB-1 downto 0)        := (others => 'X'); -- wire
			clock  : in  std_logic                                  := 'X';             -- clk
			output : out std_logic_vector(widthA+widthB-1 downto 0)                     -- wire
		);
	end component alt_dspbuilder_bus_concat_GNIIOZRPJD;

	component alt_dspbuilder_multiplexer_GNFUB4HAXG is
		generic (
			number_inputs          : natural  := 4;
			pipeline               : natural  := 0;
			width                  : positive := 8;
			HDLTYPE                : string   := "STD_LOGIC_VECTOR";
			use_one_hot_select_bus : natural  := 0
		);
		port (
			clock     : in  std_logic                    := 'X';             -- clk
			aclr      : in  std_logic                    := 'X';             -- reset
			sel       : in  std_logic_vector(1 downto 0) := (others => 'X'); -- wire
			result    : out std_logic_vector(9 downto 0);                    -- wire
			ena       : in  std_logic                    := 'X';             -- wire
			user_aclr : in  std_logic                    := 'X';             -- wire
			in0       : in  std_logic_vector(9 downto 0) := (others => 'X'); -- wire
			in1       : in  std_logic_vector(9 downto 0) := (others => 'X'); -- wire
			in2       : in  std_logic_vector(9 downto 0) := (others => 'X')  -- wire
		);
	end component alt_dspbuilder_multiplexer_GNFUB4HAXG;

	component alt_dspbuilder_gnd_GN is
		port (
			output : out std_logic   -- wire
		);
	end component alt_dspbuilder_gnd_GN;

	component alt_dspbuilder_vcc_GN is
		port (
			output : out std_logic   -- wire
		);
	end component alt_dspbuilder_vcc_GN;

	component alt_dspbuilder_port_GNA5S4SQDN is
		port (
			input  : in  std_logic_vector(7 downto 0) := (others => 'X'); -- wire
			output : out std_logic_vector(7 downto 0)                     -- wire
		);
	end component alt_dspbuilder_port_GNA5S4SQDN;

	component alt_dspbuilder_port_GN6TDLHAW6 is
		port (
			input  : in  std_logic_vector(1 downto 0) := (others => 'X'); -- wire
			output : out std_logic_vector(1 downto 0)                     -- wire
		);
	end component alt_dspbuilder_port_GN6TDLHAW6;

	component alt_dspbuilder_port_GNOC3SGKQJ is
		port (
			input  : in  std_logic_vector(23 downto 0) := (others => 'X'); -- wire
			output : out std_logic_vector(23 downto 0)                     -- wire
		);
	end component alt_dspbuilder_port_GNOC3SGKQJ;

	component gaussian_ip_GN_gaussian_ip_Fusion is
		port (
			sof_out    : out std_logic_vector(0 downto 0);                    -- wire
			ready2_in  : out std_logic;                                       -- wire
			pixel_out  : out std_logic_vector(9 downto 0);                    -- wire
			pixel2_out : out std_logic_vector(7 downto 0);                    -- wire
			pixel1_out : out std_logic_vector(7 downto 0);                    -- wire
			valid1_in  : in  std_logic                    := 'X';             -- wire
			sof1_in    : in  std_logic                    := 'X';             -- wire
			eof1_in    : in  std_logic                    := 'X';             -- wire
			valid_out  : out std_logic_vector(0 downto 0);                    -- wire
			pixel1_in  : in  std_logic_vector(7 downto 0) := (others => 'X'); -- wire
			Clock      : in  std_logic                    := 'X';             -- clk
			reset      : in  std_logic                    := 'X';             -- reset
			pixel2_in  : in  std_logic_vector(7 downto 0) := (others => 'X'); -- wire
			eof_out    : out std_logic_vector(0 downto 0)                     -- wire
		);
	end component gaussian_ip_GN_gaussian_ip_Fusion;

	component alt_dspbuilder_bus_concat_GNWZPLIVXS is
		generic (
			widthA : natural := 8;
			widthB : natural := 8
		);
		port (
			a      : in  std_logic_vector(widthA-1 downto 0)        := (others => 'X'); -- wire
			aclr   : in  std_logic                                  := 'X';             -- clk
			b      : in  std_logic_vector(widthB-1 downto 0)        := (others => 'X'); -- wire
			clock  : in  std_logic                                  := 'X';             -- clk
			output : out std_logic_vector(widthA+widthB-1 downto 0)                     -- wire
		);
	end component alt_dspbuilder_bus_concat_GNWZPLIVXS;

	component alt_dspbuilder_cast_GN6DDKTPIR is
		generic (
			round    : natural := 0;
			saturate : natural := 0
		);
		port (
			input  : in  std_logic_vector(7 downto 0) := (others => 'X'); -- wire
			output : out std_logic_vector(9 downto 0)                     -- wire
		);
	end component alt_dspbuilder_cast_GN6DDKTPIR;

	component alt_dspbuilder_cast_GN5TYUPWUA is
		generic (
			round    : natural := 0;
			saturate : natural := 0
		);
		port (
			input  : in  std_logic_vector(9 downto 0) := (others => 'X'); -- wire
			output : out std_logic_vector(7 downto 0)                     -- wire
		);
	end component alt_dspbuilder_cast_GN5TYUPWUA;

	component alt_dspbuilder_cast_GNSB3OXIQS is
		generic (
			round    : natural := 0;
			saturate : natural := 0
		);
		port (
			input  : in  std_logic_vector(0 downto 0) := (others => 'X'); -- wire
			output : out std_logic                                        -- wire
		);
	end component alt_dspbuilder_cast_GNSB3OXIQS;

	signal multiplexer2user_aclrgnd_output_wire : std_logic;                     -- Multiplexer2user_aclrGND:output -> Multiplexer2:user_aclr
	signal multiplexer2enavcc_output_wire       : std_logic;                     -- Multiplexer2enaVCC:output -> Multiplexer2:ena
	signal bus_concatenation_output_wire        : std_logic_vector(15 downto 0); -- Bus_Concatenation:output -> Bus_Concatenation2:a
	signal gaussian_ip_fusion_0_pixel_out_wire  : std_logic_vector(9 downto 0);  -- gaussian_ip_Fusion_0:pixel_out -> Multiplexer2:in0
	signal sink_data1_0_output_wire             : std_logic_vector(7 downto 0);  -- sink_data1_0:output -> gaussian_ip_Fusion_0:pixel1_in
	signal sink_data2_0_output_wire             : std_logic_vector(7 downto 0);  -- sink_data2_0:output -> gaussian_ip_Fusion_0:pixel2_in
	signal sink_eop1_0_output_wire              : std_logic;                     -- sink_eop1_0:output -> gaussian_ip_Fusion_0:eof1_in
	signal vcc_output_wire                      : std_logic;                     -- VCC:output -> sink_ready1_0:input
	signal gaussian_ip_fusion_0_ready2_in_wire  : std_logic;                     -- gaussian_ip_Fusion_0:ready2_in -> sink_ready2_0:input
	signal sink_sop1_0_output_wire              : std_logic;                     -- sink_sop1_0:output -> gaussian_ip_Fusion_0:sof1_in
	signal sink_valid1_0_output_wire            : std_logic;                     -- sink_valid1_0:output -> gaussian_ip_Fusion_0:valid1_in
	signal bus_concatenation2_output_wire       : std_logic_vector(23 downto 0); -- Bus_Concatenation2:output -> source_data_0:input
	signal switch_0_output_wire                 : std_logic_vector(1 downto 0);  -- switch_0:output -> Multiplexer2:sel
	signal gaussian_ip_fusion_0_pixel1_out_wire : std_logic_vector(7 downto 0);  -- gaussian_ip_Fusion_0:pixel1_out -> cast207:input
	signal cast207_output_wire                  : std_logic_vector(9 downto 0);  -- cast207:output -> Multiplexer2:in1
	signal gaussian_ip_fusion_0_pixel2_out_wire : std_logic_vector(7 downto 0);  -- gaussian_ip_Fusion_0:pixel2_out -> cast208:input
	signal cast208_output_wire                  : std_logic_vector(9 downto 0);  -- cast208:output -> Multiplexer2:in2
	signal multiplexer2_result_wire             : std_logic_vector(9 downto 0);  -- Multiplexer2:result -> [cast209:input, cast210:input, cast211:input]
	signal cast209_output_wire                  : std_logic_vector(7 downto 0);  -- cast209:output -> Bus_Concatenation:a
	signal cast210_output_wire                  : std_logic_vector(7 downto 0);  -- cast210:output -> Bus_Concatenation2:b
	signal cast211_output_wire                  : std_logic_vector(7 downto 0);  -- cast211:output -> Bus_Concatenation:b
	signal gaussian_ip_fusion_0_eof_out_wire    : std_logic_vector(0 downto 0);  -- gaussian_ip_Fusion_0:eof_out -> cast212:input
	signal cast212_output_wire                  : std_logic;                     -- cast212:output -> source_eop_0:input
	signal gaussian_ip_fusion_0_sof_out_wire    : std_logic_vector(0 downto 0);  -- gaussian_ip_Fusion_0:sof_out -> cast213:input
	signal cast213_output_wire                  : std_logic;                     -- cast213:output -> source_sop_0:input
	signal gaussian_ip_fusion_0_valid_out_wire  : std_logic_vector(0 downto 0);  -- gaussian_ip_Fusion_0:valid_out -> cast214:input
	signal cast214_output_wire                  : std_logic;                     -- cast214:output -> source_valid_0:input
	signal clock_0_clock_output_clk             : std_logic;                     -- Clock_0:clock_out -> [Bus_Concatenation2:clock, Bus_Concatenation:clock, Multiplexer2:clock, gaussian_ip_Fusion_0:Clock]
	signal clock_0_clock_output_reset           : std_logic;                     -- Clock_0:aclr_out -> [Bus_Concatenation2:aclr, Bus_Concatenation:aclr, Multiplexer2:aclr, gaussian_ip_Fusion_0:reset]

begin

	clock_0 : component alt_dspbuilder_clock_GNN7TLRCSZ
		port map (
			clock_out => clock_0_clock_output_clk,   -- clock_output.clk
			aclr_out  => clock_0_clock_output_reset, --             .reset
			clock     => Clock,                      --        clock.clk
			aclr      => reset                       --             .reset
		);

	source_ready_0 : component alt_dspbuilder_port_GN37ALZBS4
		port map (
			input  => source_ready, --  input.wire
			output => open          -- output.wire
		);

	sink_sop1_0 : component alt_dspbuilder_port_GN37ALZBS4
		port map (
			input  => sink_sop1,               --  input.wire
			output => sink_sop1_0_output_wire  -- output.wire
		);

	bus_concatenation : component alt_dspbuilder_bus_concat_GNIIOZRPJD
		generic map (
			widthA => 8,
			widthB => 8
		)
		port map (
			clock  => clock_0_clock_output_clk,      -- clock_aclr.clk
			aclr   => clock_0_clock_output_reset,    --           .reset
			a      => cast209_output_wire,           --          a.wire
			b      => cast211_output_wire,           --          b.wire
			output => bus_concatenation_output_wire  --     output.wire
		);

	multiplexer2 : component alt_dspbuilder_multiplexer_GNFUB4HAXG
		generic map (
			number_inputs          => 3,
			pipeline               => 0,
			width                  => 10,
			HDLTYPE                => "STD_LOGIC_VECTOR",
			use_one_hot_select_bus => 0
		)
		port map (
			clock     => clock_0_clock_output_clk,             -- clock_aclr.clk
			aclr      => clock_0_clock_output_reset,           --           .reset
			sel       => switch_0_output_wire,                 --        sel.wire
			result    => multiplexer2_result_wire,             --     result.wire
			ena       => multiplexer2enavcc_output_wire,       --        ena.wire
			user_aclr => multiplexer2user_aclrgnd_output_wire, --  user_aclr.wire
			in0       => gaussian_ip_fusion_0_pixel_out_wire,  --        in0.wire
			in1       => cast207_output_wire,                  --        in1.wire
			in2       => cast208_output_wire                   --        in2.wire
		);

	multiplexer2user_aclrgnd : component alt_dspbuilder_gnd_GN
		port map (
			output => multiplexer2user_aclrgnd_output_wire  -- output.wire
		);

	multiplexer2enavcc : component alt_dspbuilder_vcc_GN
		port map (
			output => multiplexer2enavcc_output_wire  -- output.wire
		);

	sink_sop2_0 : component alt_dspbuilder_port_GN37ALZBS4
		port map (
			input  => sink_sop2, --  input.wire
			output => open       -- output.wire
		);

	sink_data2_0 : component alt_dspbuilder_port_GNA5S4SQDN
		port map (
			input  => sink_data2,               --  input.wire
			output => sink_data2_0_output_wire  -- output.wire
		);

	sink_ready1_0 : component alt_dspbuilder_port_GN37ALZBS4
		port map (
			input  => vcc_output_wire, --  input.wire
			output => sink_ready1      -- output.wire
		);

	sink_data1_0 : component alt_dspbuilder_port_GNA5S4SQDN
		port map (
			input  => sink_data1,               --  input.wire
			output => sink_data1_0_output_wire  -- output.wire
		);

	sink_ready2_0 : component alt_dspbuilder_port_GN37ALZBS4
		port map (
			input  => gaussian_ip_fusion_0_ready2_in_wire, --  input.wire
			output => sink_ready2                          -- output.wire
		);

	switch_0 : component alt_dspbuilder_port_GN6TDLHAW6
		port map (
			input  => switch,               --  input.wire
			output => switch_0_output_wire  -- output.wire
		);

	sink_eop1_0 : component alt_dspbuilder_port_GN37ALZBS4
		port map (
			input  => sink_eop1,               --  input.wire
			output => sink_eop1_0_output_wire  -- output.wire
		);

	sink_valid2_0 : component alt_dspbuilder_port_GN37ALZBS4
		port map (
			input  => sink_valid2, --  input.wire
			output => open         -- output.wire
		);

	sink_valid1_0 : component alt_dspbuilder_port_GN37ALZBS4
		port map (
			input  => sink_valid1,               --  input.wire
			output => sink_valid1_0_output_wire  -- output.wire
		);

	sink_eop2_0 : component alt_dspbuilder_port_GN37ALZBS4
		port map (
			input  => sink_eop2, --  input.wire
			output => open       -- output.wire
		);

	source_data_0 : component alt_dspbuilder_port_GNOC3SGKQJ
		port map (
			input  => bus_concatenation2_output_wire, --  input.wire
			output => source_data                     -- output.wire
		);

	gaussian_ip_fusion_0 : component gaussian_ip_GN_gaussian_ip_Fusion
		port map (
			sof_out    => gaussian_ip_fusion_0_sof_out_wire,    --    sof_out.wire
			ready2_in  => gaussian_ip_fusion_0_ready2_in_wire,  --  ready2_in.wire
			pixel_out  => gaussian_ip_fusion_0_pixel_out_wire,  --  pixel_out.wire
			pixel2_out => gaussian_ip_fusion_0_pixel2_out_wire, -- pixel2_out.wire
			pixel1_out => gaussian_ip_fusion_0_pixel1_out_wire, -- pixel1_out.wire
			valid1_in  => sink_valid1_0_output_wire,            --  valid1_in.wire
			sof1_in    => sink_sop1_0_output_wire,              --    sof1_in.wire
			eof1_in    => sink_eop1_0_output_wire,              --    eof1_in.wire
			valid_out  => gaussian_ip_fusion_0_valid_out_wire,  --  valid_out.wire
			pixel1_in  => sink_data1_0_output_wire,             --  pixel1_in.wire
			Clock      => clock_0_clock_output_clk,             --      Clock.clk
			reset      => clock_0_clock_output_reset,           --           .reset
			pixel2_in  => sink_data2_0_output_wire,             --  pixel2_in.wire
			eof_out    => gaussian_ip_fusion_0_eof_out_wire     --    eof_out.wire
		);

	source_eop_0 : component alt_dspbuilder_port_GN37ALZBS4
		port map (
			input  => cast212_output_wire, --  input.wire
			output => source_eop           -- output.wire
		);

	source_valid_0 : component alt_dspbuilder_port_GN37ALZBS4
		port map (
			input  => cast214_output_wire, --  input.wire
			output => source_valid         -- output.wire
		);

	bus_concatenation2 : component alt_dspbuilder_bus_concat_GNWZPLIVXS
		generic map (
			widthA => 16,
			widthB => 8
		)
		port map (
			clock  => clock_0_clock_output_clk,       -- clock_aclr.clk
			aclr   => clock_0_clock_output_reset,     --           .reset
			a      => bus_concatenation_output_wire,  --          a.wire
			b      => cast210_output_wire,            --          b.wire
			output => bus_concatenation2_output_wire  --     output.wire
		);

	source_sop_0 : component alt_dspbuilder_port_GN37ALZBS4
		port map (
			input  => cast213_output_wire, --  input.wire
			output => source_sop           -- output.wire
		);

	vcc : component alt_dspbuilder_vcc_GN
		port map (
			output => vcc_output_wire  -- output.wire
		);

	cast207 : component alt_dspbuilder_cast_GN6DDKTPIR
		generic map (
			round    => 0,
			saturate => 0
		)
		port map (
			input  => gaussian_ip_fusion_0_pixel1_out_wire, --  input.wire
			output => cast207_output_wire                   -- output.wire
		);

	cast208 : component alt_dspbuilder_cast_GN6DDKTPIR
		generic map (
			round    => 0,
			saturate => 0
		)
		port map (
			input  => gaussian_ip_fusion_0_pixel2_out_wire, --  input.wire
			output => cast208_output_wire                   -- output.wire
		);

	cast209 : component alt_dspbuilder_cast_GN5TYUPWUA
		generic map (
			round    => 0,
			saturate => 0
		)
		port map (
			input  => multiplexer2_result_wire, --  input.wire
			output => cast209_output_wire       -- output.wire
		);

	cast210 : component alt_dspbuilder_cast_GN5TYUPWUA
		generic map (
			round    => 0,
			saturate => 0
		)
		port map (
			input  => multiplexer2_result_wire, --  input.wire
			output => cast210_output_wire       -- output.wire
		);

	cast211 : component alt_dspbuilder_cast_GN5TYUPWUA
		generic map (
			round    => 0,
			saturate => 0
		)
		port map (
			input  => multiplexer2_result_wire, --  input.wire
			output => cast211_output_wire       -- output.wire
		);

	cast212 : component alt_dspbuilder_cast_GNSB3OXIQS
		generic map (
			round    => 0,
			saturate => 0
		)
		port map (
			input  => gaussian_ip_fusion_0_eof_out_wire, --  input.wire
			output => cast212_output_wire                -- output.wire
		);

	cast213 : component alt_dspbuilder_cast_GNSB3OXIQS
		generic map (
			round    => 0,
			saturate => 0
		)
		port map (
			input  => gaussian_ip_fusion_0_sof_out_wire, --  input.wire
			output => cast213_output_wire                -- output.wire
		);

	cast214 : component alt_dspbuilder_cast_GNSB3OXIQS
		generic map (
			round    => 0,
			saturate => 0
		)
		port map (
			input  => gaussian_ip_fusion_0_valid_out_wire, --  input.wire
			output => cast214_output_wire                  -- output.wire
		);

end architecture rtl; -- of gaussian_ip_GN
