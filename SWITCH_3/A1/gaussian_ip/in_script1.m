if (length(dir('image.bmp'))>0)
	orig_im = imread('image.bmp');
    orig_1 = orig_im(:,:,1);
    orig_2 = orig_im(:,:,3);
    
    orig_1 = padarray(orig_1, [14 14], 0, 'both');
    orig_2 = padarray(orig_2, [14 14], 0, 'both');
	[nrws ncls] = size(orig_2);
    offset = 1300;
	t = 1:ncls*nrws + offset;
	pixel_in.signals.values = (1:ncls*nrws+offset)';
    valid_in.signals.values = (1:ncls*nrws+offset)';
	sof_in.signals.values = (1:ncls*nrws+offset)';
    eof_in.signals.values = (1:ncls*nrws+offset)';
    
	pixel_in.time = t;
    valid_in.time = t;
	sof_in.time = t;
    eof_in.time = t;
    
    pixel_in.signals.values(1:end) = 0;
    valid_in.signals.values(1:end) = 0;
    sof_in.signals.values(1:end) = 0;
    eof_in.signals.values(1:end) = 0;
    
	for i=1:nrws
	    for j=1:ncls
	          pixel_in.signals.values((i-1)*ncls+j+1) = orig_2(i,j);
              valid_in.signals.values((i-1)*ncls+j+1) = 1;
        end
    end
    eof_in.signals.values(1) = 1;
	sof_in.signals.values(2) = 1;
    eof_in.signals.values(ncls*nrws+1) = 1;
    valid_in.signals.values(ncls*nrws+1:ncls*nrws+offset) = 1;
else
	alt_dspbuilder_error('Unable to locate the image image_in.jpg');
	disp(['  > Unable to locate the image image_in.jpg']);
end

h = [1, 4, 6, 4, 1; ...
     4, 16, 24, 16, 4; ...
     6, 24, 36, 24, 6; ...
     4, 16, 24, 16, 4; ...
     1, 4, 6, 4, 1];
h = h/256;
h = round(h, 7);
q1 = round(filter2(h, orig_2, 'valid'));
diff = double(orig_2(3:506, 3:666)) - q1;

h2 = [1, 0, 4, 0, 6, 0, 4, 0, 1; ...
      0, 0, 0, 0, 0, 0, 0, 0, 0; ...
      4, 0, 16, 0, 24, 0, 16, 0, 4; ...
      0, 0, 0, 0, 0, 0, 0, 0, 0; ...
      6, 0, 24, 0, 36, 0, 24, 0, 6; ...
      0, 0, 0, 0, 0, 0, 0, 0, 0; ...
      4, 0, 16, 0, 24, 0, 16, 0, 4; ...
      0, 0, 0, 0, 0, 0, 0, 0, 0; ...
      1, 0, 4, 0, 6, 0, 4, 0, 1];
h2 = h2/256;
h2 = round(h2, 7);
q2 = round(filter2(h2, q1, 'valid'));
diff = diff(5:500, 5:660) + q1(5:500, 5:660) - q2;

h3 = [1, 0, 0, 0, 4, 0, 0, 0, 6, 0, 0, 0, 4, 0, 0, 0, 1; ...
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
      4, 0, 0, 0, 16, 0, 0, 0, 24, 0, 0, 0, 16, 0, 0, 0, 4; ...
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
      6, 0, 0, 0, 24, 0, 0, 0, 36, 0, 0, 0, 24, 0, 0, 0, 6; ...
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
      4, 0, 0, 0, 16, 0, 0, 0, 24, 0, 0, 0, 16, 0, 0, 0, 4; ...
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
      1, 0, 0, 0, 4, 0, 0, 0, 6, 0, 0, 0, 4, 0, 0, 0, 1];
h3 = h3/256;
h3 = round(h3, 7);
q3 = round(filter2(h3, q2, 'valid'));
diff = diff(9:488, 9:648) + q2(9:488, 9:648) - q3;

qout = double(orig_1(15:494, 15:654)) + diff;
qout = uint8(qout);
% orig_2_cropped = orig_2(3:478,3:638);
% qout = int16(orig_1(3:478, 3:638)) + int16(orig_2_cropped)-int16(q);
% qout = uint8(qout);
% 
% h2 = [1, 0, 4, 0, 6, 0, 4, 0, 1; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       4, 0, 16, 0, 24, 0, 16, 0, 4; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       6, 0, 24, 0, 36, 0, 24, 0, 6; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       4, 0, 16, 0, 24, 0, 16, 0, 4; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       1, 0, 4, 0, 6, 0, 4, 0, 1];
% h2 = h2/256;
% h2 = ufi(h2,26,18);
% q2 = ufi(zeros(468, 628),26,18);
% q2 = round(filter2(h2, q, 'valid'));
% qout2 = int16(qout(5:472, 5:632)) + int16(q(5:472, 5:632))-int16(q2);
% qout2 = uint8(qout2);
% 
% h3 = [1, 0, 0, 0, 4, 0, 0, 0, 6, 0, 0, 0, 4, 0, 0, 0, 1; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       4, 0, 0, 0, 16, 0, 0, 0, 24, 0, 0, 0, 16, 0, 0, 0, 4; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       6, 0, 0, 0, 24, 0, 0, 0, 36, 0, 0, 0, 24, 0, 0, 0, 6; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       4, 0, 0, 0, 16, 0, 0, 0, 24, 0, 0, 0, 16, 0, 0, 0, 4; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       1, 0, 0, 0, 4, 0, 0, 0, 6, 0, 0, 0, 4, 0, 0, 0, 1];
% h3 = h3/256;
% h3 = ufi(h3,26,18);
% q3 = ufi(zeros(452, 612),26,18);
% q3 = round(filter2(h3, q2, 'valid'));
% qout3 = int16(qout2(9:460, 9:620)) + int16(q2(9:460, 9:620))-int16(q3);
% qout3 = uint8(qout3);
% 
% h4 = [1, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 1; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       4, 0, 0, 0, 0, 0, 0, 0, 16, 0, 0, 0, 0, 0, 0, 0, 24, 0, 0, 0, 0, 0, 0, 0, 16, 0, 0, 0, 0, 0, 0, 0, 4; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       6, 0, 0, 0, 0, 0, 0, 0, 24, 0, 0, 0, 0, 0, 0, 0, 36, 0, 0, 0, 0, 0, 0, 0, 24, 0, 0, 0, 0, 0, 0, 0, 6; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       4, 0, 0, 0, 0, 0, 0, 0, 16, 0, 0, 0, 0, 0, 0, 0, 24, 0, 0, 0, 0, 0, 0, 0, 16, 0, 0, 0, 0, 0, 0, 0, 4; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0; ...
%       1, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 1];
% h4 = h4/256;
% h4 = ufi(h4,26,18);
% q4 = ufi(zeros(420, 580),26,18);
% q4 = round(filter2(h4, q3, 'valid'));
% qout4 = int16(qout3(17:436, 17:596)) + int16(q3(17:436, 17:596))-int16(q4);
% qout4 = uint8(qout4);
% 
% figure, imshow(qout4);
% %h1 = ufi(zeros(9, 9), 26, 18);
% %h1(1:2:end, 1:2:end) = h;
% %q1 = ufi(zeros(468, 628),26,18);
% %q1 = round(filter2(h1, q, 'valid'));
% 
% %h2 = ufi(zeros(17, 17), 26, 18);
% %h2(1:4:end, 1:4:end) = h;
% %q2 = ufi(zeros(452, 612),26,18);
% %q2 = round(filter2(h2, q1, 'valid'));
% 
% %h3 = ufi(zeros(33, 33), 26, 18);
% %h3(1:8:end, 1:8:end) = h;
% %q3 = ufi(zeros(420, 580),26,18);
% %q3 = round(filter2(h3, q2, 'valid'));
% 
